:au BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit' 
\ |   exe "normal! g`\""
\ | endif

set pastetoggle=<F2>

"colorscheme monokai
set notermguicolors
syntax sync minlines=1000
set redrawtime=10000

call plug#begin('~/.local/share/nvim/plugged')
Plug 'https://github.com/crusoexia/vim-monokai.git', { 'do': 'cp colors/monokai.vim ~/.config/nvim/colors/' }
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-refactor'
Plug 'https://github.com/neomake/neomake.git'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()
""call neomake#configure#automake('w')
""
""let g:airline_powerline_fonts = 1
""let g:airline_theme='angr'
""
""" TextEdit might fail if hidden is not set.
""set hidden
""
""" Give more space for displaying messages.
""set cmdheight=2
""
""" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
""" delays and poor user experience.
""set updatetime=300
""
""" Don't pass messages to |ins-completion-menu|.
""set shortmess+=c
""
""" Always show the signcolumn, otherwise it would shift the text each time
""" diagnostics appear/become resolved.
""if has("nvim-0.5.0") || has("patch-8.1.1564")
""  " Recently vim can merge signcolumn and number column into one
""  set signcolumn=number
""else
""  set signcolumn=yes
""endif
""
""lua << EOF
""require'nvim-treesitter.configs'.setup {
""  -- A list of parser names, or "all" (the five listed parsers should always be installed)
""  ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "python", "javascript", "php" },
""
""  -- Install parsers synchronously (only applied to `ensure_installed`)
""  sync_install = false,
""
""  -- Automatically install missing parsers when entering buffer
""  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
""  auto_install = true,
""
""  -- List of parsers to ignore installing (or "all")
""  ignore_install = {},
""
""  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
""  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!
""
""  highlight = {
""    enable = true,
""
""    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
""    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
""    -- the name of the parser)
""    -- list of language that will be disabled
""    disable = {},
""    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
""    disable = function(lang, buf)
""        local max_filesize = 100 * 1024 -- 100 KB
""        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
""        if ok and stats and stats.size > max_filesize then
""            return true
""        end
""    end,
""
""    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
""    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
""    -- Using this option may slow down your editor, and you may see some duplicate highlights.
""    -- Instead of true it can also be a list of languages
""    additional_vim_regex_highlighting = false,
""  },
""
""  indent = {
""    enable = true
""  },
""
""  refactor = {
""    highlight_definitions = {
""      enable = true,
""      -- Set to false if you have an `updatetime` of ~100.
""      clear_on_cursor_move = true,
""    },
""    navigation = {
""      enable = true,
""      -- Assign keymaps to false to disable them, e.g. `goto_definition = false`.
""      keymaps = {
""        goto_definition = "gnd",
""        list_definitions = "gnD",
""        list_definitions_toc = "gO",
""        goto_next_usage = "<a-*>",
""        goto_previous_usage = "<a-#>",
""      },
""    },
""  },
""}
""EOF

