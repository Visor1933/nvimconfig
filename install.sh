#!/bin/sh
PIDS=""
NVIMCONF="$HOME/.config/nvim"
NVIM="$(test -n "$1" && echo "$1" || echo "nvim")"
echo create directory structure
for d in "colors" "after/ftplugin" "plugged"
do mkdir -pv "$NVIMCONF/$d"
done
echo "install neovim"
curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
echo copying files
cp -r ftplugin/* "$NVIMCONF/after/ftplugin/"
cp init.vim "$NVIMCONF/"
echo installing plugins
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
$NVIM +:PlugInstall +:qa
sed -i -e 's/^""//' "$NVIMCONF/init.vim"
